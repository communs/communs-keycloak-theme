
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 
<html>
<head>
    <title>Les Communs - login</title>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" href="welcome-content/favicon.ico" type="image/x-icon">

    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
</head>

<body>
  <div id="logos">
    <a href="http://lescommuns.org" target="_blank">Les communs</a>
    <a href="https://jardiniersdunous.org" target="_blank">Jardiniers du Nous</a>
    <a href="https://presdecheznous.fr" target="_blank">Près de chez nous</a>
    <a href="https://www.resiway.org/resipedia.fr" target="_blank">RésiPedia</a>
    <a href="https://printemps-education.org" target="_blank">Le Printemps de l‘Éducation</a>
    <a href="https://www.virtual-assembly.org" target="_blank">Assemblée virtuelle</a>
  </div>

  <div class="card-pf">
    <header></header><h1 class="kc-page-title">Bienvenue sur la plateforme d'identification partagée du mouvement des Communs !</h1>
    <p>Celle-ci vous permet d'utiliser un seul compte utilisateur pour vous identifier sur les plateformes du réseau (voir logos ci-dessus).</p>
    <ul>
      <li><b>Si vous êtes visiteur</b>, il suffit de créer votre compte en passant par une des plateformes, et vous pourrez ensuite vous connecter à toutes les autres avec le même compte.</li>
      <li><b>Si vous êtes développeur</b> et que vous souhaitez faire rejoindre votre site web à l'écosytème, n‘hésitez pas à <a href="https://marc.fun/" target="_blank">contacter Marc</a>.</li>
    </ul>

    <p style="text-align: center;"><a href="https://login.lescommuns.org/auth/realms/master/account" class="btn">Créer mon compte<br>Me (dé)connecter<br>Changer mon mot de passe</a></p>
  </div>

  <a href="https://infomaniak.com" target="_blank">Hébergé par Infomaniak</a>
</body>
</html>
