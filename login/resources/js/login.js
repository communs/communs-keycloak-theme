document.addEventListener("DOMContentLoaded", function(event) { 
	document.getElementById("kc-header").innerHTML = '<div id="logos">    <a href="http://lescommuns.org" target="_blank">Les communs</a>    <a href="https://jardiniersdunous.org" target="_blank">Jardiniers du Nous</a>    <a href="https://presdecheznous.fr" target="_blank">Près de chez nous</a>    <a href="https://www.resiway.org/resipedia.fr" target="_blank">RésiPedia</a>    <a href="https://printemps-education.org" target="_blank">Le Printemps de l‘Éducation</a>  <a href="https://www.virtual-assembly.org" target="_blank">Assemblée virtuelle</a>   </div>';
	registerLink = document.querySelector("#kc-registration a");
	if (registerLink !== null) {
		registerLink.classList.add("btn");
		registerLink.classList.add("btn-warning");
		registerLink.classList.add("btn-block");
		registerLink.classList.add("btn-lg");
	}


	// Page to reset password
	var resetPasswordForm = document.getElementById("kc-reset-password-form");
	if (resetPasswordForm !== null) {
		var el = document.createElement("p");
		el.style.cssText = 'color: red;';
		el.innerHTML = "IMPORTANT : si vous ne recevez pas le mail, c'est qu'il n'y a pas de compte correspondant à cette adresse mail. Si c'est le cas, essayez avec une autre adresse mail ou créez un compte (bouton \"S'enregistrer\").";
		resetPasswordForm.parentNode.insertBefore(el, resetPasswordForm.nextSibling);
	}


	// Page to register
	if (document.getElementById("kc-register-form") !== null || document.getElementById("kc-passwd-update-form")) {
		var el = document.createElement("span");
		el.style.cssText = 'color: #2a00ff;';
		el.innerHTML = "Le mot de passe doit contenir au moins 8 caractères, une majuscule et un chiffre.";
		var passwordInput = document.getElementById("password");
		if (passwordInput !== null) {
			passwordInput.parentNode.insertBefore(el, passwordInput.nextSibling);
		}
	}


	// Check "Rember me" checkbox by default
	var rememberMe = document.getElementById("rememberMe");
	if (rememberMe !== null) {
		document.getElementById("rememberMe").checked = true;
	}


	/**
	 * Add current URL to HTML tag in data-url attribute
	 */
	// Returns an URL formated for CSS :
	// - removes base URL, params and first slash 
	// - replaces slash with ___
	// E.g.: `http://my-humhub.com/s/space-1/custom_pages/view?id=3` will return `s___space-1___custom_pages___view`
	var getUrlForCss = function (url, baseUrl) {
		// Remove base URL and first slash
		urlForCss = url.substring(baseUrl.length);
		// Remove params
		if (urlForCss.includes('?')) {
			urlForCss = urlForCss.substring(0, urlForCss.indexOf('?'));
		}
		// If ends with slash, remove it
		if (urlForCss.lastIndexOf('/') == urlForCss.length-1) {
			urlForCss = urlForCss.substring(0, urlForCss.lastIndexOf('/'));
		}
		// Replace / with ___ and remove all special chars
		return urlForCss.replace(/\//gi, '___').replace(/[^-\w\s]/gi, '');
	}
	var currentUrl = window.location.href;
	var urlForCss = getUrlForCss(currentUrl, 'https://');
	document.getElementsByTagName('html')[0].setAttribute('data-url', urlForCss);
});